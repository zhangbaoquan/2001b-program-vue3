import axios, { AxiosInstance, AxiosRequestConfig } from 'axios';
import { ElLoading, ElMessage } from 'element-plus'
import { LoadingInstance } from "element-plus/lib/components/loading/src/loading";
import LocalSto from "../uilts/uilts";
//定义是否需要显示lodding效果
const DEFAUT_LOADING = true;
// axios.defaults.withCredentials = true;
interface HTTPRequsetConfig extends AxiosRequestConfig {
    isShowLoading?: boolean
}
class HttpRequset {

    static get<T>(arg0: { url: string; params: any; }) {
        throw new Error('Method not implemented.');
    }
    instance: AxiosInstance;
    isShowLoading: boolean = false;
    isLoading?: LoadingInstance;
    constructor(config: HTTPRequsetConfig) {
        this.instance = axios.create(config);//创建axios实例
        this.isShowLoading = config.isShowLoading || DEFAUT_LOADING;
        //添加请求拦截器
        this.instance.interceptors.request.use(configs => {
            //判断是否需要加载loading效果
            if (this.isShowLoading) {
                //发送请求之前显示loading效果
                this.isLoading = ElLoading.service({
                    lock: true,
                    text: '正在请求中······',
                    background: 'rgba(0,0,0,0.6)',

                })
            }
            this.isShowLoading = false;
            //获取token 放在请求头中
            const token = LocalSto.getCache('token');
            return {
                ...configs,
                headers: {
                    ...config.headers,
                    Authorization: token,
                }
            }
        }, error => {
            return Promise.reject(error);
        })
        //添加响应拦截器 在服务器响应之后 关闭loading效果
        this.instance.interceptors.response.use(response => {
            //关闭loading效果
            this.isLoading?.close();
            return response;

        }, error => {
            this.isLoading?.close();
            const { code } = error.response;
            switch (code) {
                case 400:
                    ElMessage.error('请求失败(错误的请求)')
                    break;
                case 401:
                    ElMessage.error('没有访问权限')
                    break;
            }
            //针对后端响应的http状态码进行处理
            return Promise.reject(error);
        })
    }
    request<T>(config: HTTPRequsetConfig): Promise<T> {
        return new Promise<T>((resolve, reject) => {
            this.instance
                .request<any, T>(config)
                .then(res => {
                    //更改isShowLoading的值
                    this.isShowLoading = DEFAUT_LOADING;
                    resolve(res)
                })
                .catch(error => {
                    this.isShowLoading = DEFAUT_LOADING;
                    reject(error)
                    return error;
                })
        })
    }
    get<T>(config: HTTPRequsetConfig): Promise<T> {
        return this.request<T>({
            ...config,
            method: 'GET'
        })
    }
    post<T>(config: HTTPRequsetConfig): Promise<T> {
        return this.request<T>({
            ...config,
            method: 'POST'
        })
    }
    // 删除
    delete<T>(config: HTTPRequsetConfig): Promise<T> {
        return this.request<T>({
            ...config,
            method: 'DELETE'
        })
    }
    put<T>(config: HTTPRequsetConfig): Promise<T> {
        return this.request<T>({
            ...config,
            method: 'PUT'
        })
    }
}
export default HttpRequset;