import HttpRequest from "./request";

 const request = new HttpRequest({
  baseURL: import.meta.env.BASE_URL,
  timeout: 20000,
});

export default request
