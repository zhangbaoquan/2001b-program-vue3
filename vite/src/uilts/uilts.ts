

class localSto{
    
   
  setCache(key:string,value:any){
       if(typeof value==="object"){
           value=JSON.stringify(value)
       }
       window.localStorage.setItem(key, value)
   };
   getCache(key:string){
       const value=window.localStorage.getItem(key)
       if(! value) return null
       try{
           return JSON.parse(value)
       }catch(error){
           return value
       }
   };
   
   removeCache(key:string){
      window.localStorage.removeItem(key)
   };

   clearCache(){
      window.localStorage.clear()
   };
}
export default new localSto()