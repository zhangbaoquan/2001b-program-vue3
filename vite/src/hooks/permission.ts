// XLSX 用于生成表格的  npm官网上面可查  这个表格比较成熟 需要下载


import { utils, writeFile, BookType } from 'xlsx'


const useJsonToExcel = (options: {
    data: any[];
    header: any,
    fileName: string,
    // -------------,
    bookType: BookType
}) => {
    //  创建一个工作簿
    const exSheet = utils.book_new();
    // 创建工作表 画表  判断他有没有
    if (options.header) {
        //   转换data
        options.data = options.data.map((item, index) => {
            // 定义一个对象 放item内容
            const obj: any = {}
            for (const key in item) {
                obj[options.header[key]] = item[key]
            }
            return obj;
        })
    }
    // 下一步创建sheet页
    const ts = utils.json_to_sheet(options.data);

    // 下一步把他放进去工作簿里面
    utils.book_append_sheet(exSheet, ts)

    // 下一步把数据写到创建的表里面
    writeFile(exSheet,options.fileName,{
       bookType:options.bookType
    })
}

export default useJsonToExcel