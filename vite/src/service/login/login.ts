import request from '../../uilts/index'
import { ResponseData, LoginResponseData, USerinfo, NavList, } from './types'


// 获取导航列表
export const menu_List = (data?: any) => {
 return request.get<ResponseData<NavList>>({
    url: '/api/sys/menu/nav',
    data
  })
}


// 获取用户信息
export const _userInfo = (data?: any) => {
  return request.get<ResponseData<USerinfo>>({
    url: '/api/sys/user/info',
    data
  })
}



// 登录的接口
export const _login = (params: any) => (
  request.post<ResponseData<LoginResponseData>>({
    url: `/api/login?grant_type=admin`,
    data: params
  })
)


// 退出登录的接口
export const _ExitLogin = (params?: any) => (
  request.post<ResponseData<LoginResponseData>>({
    url: `/api/sys/logout`,
    data: params
  })
)


