
export interface ResponseData<T>{
    status:number,
    data:T
}

export interface LoginResponseData{
    access_token:string,
    authorities:any[],
    expires_in:number,
    refresh_token:string,
    shopId:number,
    token_type:string,
    userId:number
}


export interface USerinfo{
    createTime:string,
    email:string,
    mobile:string,
    roleIdList:number[],
    shopId:number,
    status:number,
    userId:number,
    username:string
}



export interface authorititem{
   authority:string,
}

export interface MENUlist{
    authorities:authorititem[],
    name:string,
    orderNum:number,
    parentId:number,
    parentName:string,
    perms:string,
    type:number,
    url:string
}

export interface NavList{
    authorities:authorititem[],
    menuList:MENUlist[]
}