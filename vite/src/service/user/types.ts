
export interface Data<T>{
    status:number,
    data:T
}

export interface USERlist{
    modifyTime:string,
    nickName:string,
    pic:string,
    userRegtime:string,
    userId:string,
    status:number,
    data:any,
    records:any,
    current:number,
    size:number,
    total:number,
    pages:number
}
