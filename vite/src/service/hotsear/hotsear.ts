import request from '../../uilts/index';
import { ResponseData, records } from './type';

export const hotsearshop = (params?: any) => {
  return request.get<ResponseData<records>>({
    url: '/api/admin/hotSearch/page',
    data: params,
  });
};

export const hotseardetele = (params: any) => {
  return request.delete<ResponseData<records>>({
    url: `/api/admin/hotSearch`,
    //@ts-ignore
    data: params,
  });
};

export const hotsearput = (params: any) => {
  return request.put<ResponseData<records>>({
    url: '/api/admin/hotSearch',
    data: params,
  });
};

export const hotsearadd = (params?: any) => {
  return request.post<ResponseData<records>>({
    url: '/api/admin/hotSearch',
    data: params,
  });
};
