import request from '../../uilts/index'
import { Shuead, ShueadList, ShueadListItem } from './type'
//参数管理数据

// 获取数据
export const _shueadList = (data?: any) => {
  return request.get<Shuead<ShueadList>>({
    url: '/api/sys/config/page',
    data
  })
}

//删除
 export const _deleteShueadList = (params:any) => {
   return request.delete<Shuead<ShueadList>>({
     url: `/api/sys/config`,
     data:params
   })
 }

//增加
export const _addShueadList = (data?: any) => {
  return request.post<Shuead<ShueadList>>({
    url: '/api/sys/config',
    data
  })
}
//编辑
export const _editShueadList = (params?: any) => {
  return request.put<Shuead<ShueadList>>({
    url: `/api/sys/config`,
    data:params
  })
}
export const _editssShueadList = (params?: any) => {
  return request.get<Shuead<ShueadList>>({
    url: `/api/sys/config/info/${params.id}`,
    data:params
  })
}