import request from '../../uilts/index'
//地区数据
import {Area, AreaList,AreaListItem} from './type'


// 获取数据
export const _areaList = (list?:any) => {
    return request.get<Area<AreaListItem>>({
      url: '/api/admin/area/list',
      data:list
    })
  }