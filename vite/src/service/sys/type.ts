//日志数据
export interface UsersListItem{
    createDate: string,
    id: number,
    ip: string,
    method:string,
    operation: string,
    params: string,
    time: number,
    username: string
}
export interface UsersList{
    current: number,
	pages: number,
    searchCount: boolean,
	size:number,
	total: number,
    records:UsersListItem[],
} 
export interface Users<T>{
    data:T,
}
//参数管理数据
export interface Shuead<T>{
    data:T,
}
export interface ShueadList{
    current: number,
	pages: number,
    searchCount: boolean,
	size:number,
	total: number,
    records:ShueadListItem[],
}
export interface ShueadListItem{
    id: number,
    paramKey: string,
    paramValue: string,
    remark: string
}
//定时数据
export interface Role<T>{
    data:T,
}
export interface RoleList{
    current: number,
	pages: number,
    searchCount: boolean,
	size:number,
	total: number,
    records:RoleListItem[],
}
export interface RoleListItem{
    beanName:string,
    createTime:string,
    cronExpression:string,
    jobId:number,
    methodName:string,
    params:string,
    remark:string,
    status:number
}
//角色管理
export interface Log<T>{
    data:T,
}
export interface LogList{
    current: number,
	pages: number,
    searchCount: boolean,
	size:number,
	total: number,
    records:LogListItem[],
}
export interface LogListItem{
    roleId: number,
    roleName: string,
    remark: string,
    menuIdList: any,
    createTime: string
}
//管理员管理
export interface Config<T>{
    data:T,
}
export interface ConfigList{
    current: number,
	pages: number,
    searchCount: boolean,
	size:number,
	total: number,
    records:ConfigListItem[],
}
export interface ConfigListItem{
    createTime:string,
    email:string
    mobile:string,
    roleIdList:any,
    shopId:number
    status:number,
    userId:number
    username:string
}
//地区 !!!
export interface Area<T>{
    list:T,
     data:AreaListItem[],
}
export interface AreaList{
    config: number,
    statusText: string,
    status:number,
    request:any,
    headers:any

}
export interface AreaListItem{
    areaId: number,
    areaName: string,
    parentId: number,
    level: number,
    areas: any
}
//菜单 !!

export interface Meun<T>{
    data:T,
}
export interface MeunList{
    current: number,
	pages: number,
    searchCount: boolean,
	size:number,
	total: number,
    records:MeunListItem[],
}
export interface MeunListItem{
    menuId: number,
    parentId: number,
    parentName: any,
    name: string,
    url: any,
    perms: string,
    type: number,
    icon: any,
    orderNum: number,
    list: any,
}