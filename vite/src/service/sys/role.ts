import request from '../../uilts/index'
import {Role,RoleList} from './type'
//定时任务数据

// 获取数据
export const _rloeList = (data?:any) => {
    return request.get<Role<RoleList>>({
      url: '/api/sys/schedule/page',
      data
    })
  }
  //添加
  export const _addRloeList = (data?:any) => {
    return request.post<Role<RoleList>>({
      url: '/api/sys/schedule',
      data
    })
  }
  //删除
  export const _deleteRloeList = ({data}:any) => {
    return request.post<Role<RoleList>>({
      url: '/api/sys/schedule',
      data
    })
  }