import request from '../../uilts/index'
import { Log,LogList,LogListItem} from './type'
//角色数据

// 获取数据
export const _logList = (data?:any) => {
    return request.get<Log<LogList>>({
      url: '/api/sys/role/page',
      data
    })
  }
//删除
export const _deleteLogList= ({data}:any) => {
  return request.delete<Log<LogList>>({
    url: `/api/sys/role`,
    data
  })
}