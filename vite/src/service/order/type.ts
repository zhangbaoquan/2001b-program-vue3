export interface ResponseData<T>{
    status:number,
    data:T
}

interface ORFETRMLSF{
    commSts:number,
    orderItemId:number,
    orderNumber:string,
    pic:string,
    price:number,
    prodCount:number,
    prodId:number,
    prodName:string,
    productTotalAmount:number,
    recTime:string,
    shopId:number,
    skuId:number,
    skuName:string,
    userId:string,
}

export interface ORDERLIST{
    actualTotal:number,
    addrOrderId:number,
    createTime:string,
    deleteStatus:number,
    freightAmount:number,
    isPayed:number,
    orderId:number,
    orderItems:ORFETRMLSF[],
    orderNumber:string,
    prodName:string,
    productNums:number,
    refundSts:number,
    shopId:number,
    status:number,
    total:number,
    updateTime:string,
    records:any,
    current:number,
    pages:number,
    size:number,
    pic:string,
} 