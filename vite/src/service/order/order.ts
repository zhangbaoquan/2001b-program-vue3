import request  from '../../uilts/index'
import {ResponseData,ORDERLIST} from './type'

export const _OrderList = (data?: any) => {
  return  request.get<ResponseData<ORDERLIST>>({
        url: `/api/order/order/page`,
        params:{...data}
    })
}