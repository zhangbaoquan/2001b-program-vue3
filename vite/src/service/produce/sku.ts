import request from "../../uilts";
export const get_skuList=(data:any)=>{
    return request.get({
        url:'/api/prod/spec/page',
        method:'GET',
        params:data
    })
}
export const edit_skuList=(data:any)=>{
    return request.put({
        url:'/api//prod/spec',
        method:'PUT',
        data
    })
}
export const del_skuList=(id:number)=>{
    return request.delete({
        url:`/api/prod/spec/${id}`,
        method:'DELEte'
    })
}
export const add_skuList=(data:any)=>{
    return request.post({
        url:'/api/prod/spec',
        method:'POST',
        data
    })
}