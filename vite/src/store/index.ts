import useLoginStore from './module/login'
import usenoticeStore from './module/notice'
import useswiperStore from "./module/swiper"
import usepickUpPointsStore from "./module/pickUpPoints"
import usetoSearchStore from "./module/toSearch"
import usefeightStore from "./module/feight"
import scheduleStore from './module/schedule'
//董壮壮
import useproduceStore from './module/produce'
import useproductStore from './module/product'
export default function useStore () {
    return {
        login:useLoginStore(),
        notice: usenoticeStore(),
        toSearch: usetoSearchStore(),
        swiper: useswiperStore(),
        pickUpPoints: usepickUpPointsStore(),
        feight: usefeightStore(),
        schedule:scheduleStore(),
         //董壮壮
         produce:useproduceStore(),
         product: useproductStore(),
    }
}
