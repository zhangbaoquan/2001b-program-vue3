import {defineStore} from 'pinia'
import {swiperdeleteone,swiperList} from "../../service/swiper/swiper"

const swiperStore:any=defineStore('swiperStore',{
    state(){
        return{
           data:[],
           dialogVisible:false,
           delId:null,
           current:1,
           size:10
        }
    },
    actions:{
        async getList(){
            const res:any=await swiperList({
              t:1666872673528,
              current:this.current,
              size:this.size
            })
            this.data=res.data.records      
    
        },
        async truedelone(){
            await swiperdeleteone([this.delId])
            this.getList()
        },

        async swiperupdate(){
            console.log(this,"this");
            
        } 
    }
})
export default swiperStore


