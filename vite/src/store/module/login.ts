import { defineStore } from 'pinia'
// 将接口数据存进去厂库
import { _login, _userInfo, menu_List } from '../../service/login/login'
import { _OrderList } from '../../service/order/order'
// 拿一下封装的本地
import locaStor from '../../uilts/uilts'
// 这个登录是每个人的权限不一样 登录进去能够看到的页面也各不相同 也能够设置他的权限在里面
const login = defineStore('login', {
    state() {
        // 里面定义变量将后台返回来的值 存进去仓库
        return {
            // 登录token值
            token: locaStor.getCache('token') || "",
            // 权限列表
            authorities: locaStor.getCache('authorities') || [],
            // 用户的信息
            userInfo: locaStor.getCache('userInfo') || {},
            // 获取menu导航
            menuList: locaStor.getCache('menuList') || [],



            OrderList: [],
            total:0,
            size:8,
            current:1,
            pages:0
        }
    },
    actions: {
        // payload返回的值 state当前的
        async LoginName(payload: any) {
            const result = await _login(payload)
            // 本地存进去的token 跟后台返回的token拼接就是我们生成的token
            locaStor.setCache('token', `${result.data.token_type}${result.data.access_token}`)
            // 存进去本地的权限列表
            locaStor.setCache('authorities', result.data.authorities)
            // 令牌类型token_type  access_token访问令牌
            this.token = `${result.data.token_type}${result.data.access_token}`
            // console.log(result);  //打印看看后台返回来的都是设么值

            // 获取用户信息 接口请求成功了  但是数据没有打印到
            const userInfo = await _userInfo(payload)
            this.userInfo = userInfo.data
            locaStor.setCache('userInfo', userInfo.data)
            console.log(userInfo, 'userInfo');


            // 获取menu列表
            const menuList = await menu_List()
            this.menuList = menuList.data.menuList
            locaStor.setCache('menuList', menuList.data.menuList)
            console.log(menuList, 'menuList')

        },

        async getOrder(payload: any) {
            const { data } = await _OrderList(payload)
            this.OrderList = data.records
            this.total = data.total
            this.pages = data.pages
        }
    }
})

export default login