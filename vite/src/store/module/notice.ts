import { th } from 'element-plus/es/locale'
import {defineStore} from 'pinia'
import {deleteone,get_noticeList} from "../../service/notice/notice"
const noticeStore:any=defineStore('notice',{
    state(){
        return{
           data:[],
           dialogVisible:false,
           delId:null,
           current:1,
           size:10
        }
    },
    actions:{
        async getList(){
            const res:any=await get_noticeList({
              t:1666872673528,
              current:this.current,
              size:this.size
            })
            this.data=res.data.records
        },
        async truedelone(){
            await deleteone(this.delId)
            this.getList()
        }
        
    }
})
export default noticeStore