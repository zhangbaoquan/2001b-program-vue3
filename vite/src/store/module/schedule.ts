import { defineStore } from 'pinia'
import { _addShueadList,_deleteShueadList, _editShueadList, _shueadList } from '../../service/sys/shuead'

const scheduleStore = defineStore('schedule', {
    state() {
        return {
            data: [],
            id: null,
            current: 1,
            size: 10,
        }
    },
    actions: {
       async deleteShueadList() {
           await _deleteShueadList([this.id])
           
        }


    }
})
export default scheduleStore