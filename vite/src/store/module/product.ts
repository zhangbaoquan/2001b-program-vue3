import {defineStore} from 'pinia'
import {get_skuList,edit_skuList,add_skuList} from '../../service/produce/sku'
const productStore:any=defineStore('product',{
    state(){
        return{
            groupList:[],
            produceList:[],
            classList:[],
            commentList:[],
            skuList:[],
            skuCurrent:1,
            skuSize:10,
            skuDetail:{},
            skuSearch:{},
            skuUser:{},
            skuDisplay:false,
            addskuDisplay:false,
            skuDisplayList:[{propId:0,propName:'',prodPropValues:[]}] as any,
            addskuDisplayList:[{propId:0,propName:'',prodPropValues:[{}]}] as any
        }
    },
    actions:{
        // sku规格
        async searchSkuList(row:any){
            const res:any= await get_skuList({
                current:this.skuCurrent,
                size:this.skuSize,
                propName:row.propName
            })
            this.skuDetail=res.data
            this.skuList=res.data.records
        },
        async get_List(){
            const res:any= await get_skuList({
                current:this.skuCurrent,
                size:this.skuSize
            })
            this.skuDetail=res.data
            this.skuList=res.data.records
        },
        currentChange(page:number){
            this.skuCurrent=page
            this.get_List()
        },
        sizeChange(page:number){
            this.skuCurrent=1
            this.skuSize=page
            this.get_List()
        },
        clearPropName(row:any){
            this.skuSearch=''
        },
        editDisplay(row:any){
            console.log(row);
            this.skuDisplay=true
            this.skuDisplayList[0].propId=row.propId
            this.skuDisplayList[0].propName=row.propName
            this.skuDisplayList[0].prodPropValues=row.prodPropValues
        },
        editskuList(value:any){
            edit_skuList(value[0])
            this.skuDisplay=false
        },
        addDisplay(){
            this.addskuDisplay=true
        },
        addskuList(value:any){
            add_skuList(value[0])
            this.addskuDisplay=false
            this.get_List()
        },
        addInput(value:any){
            if(value[0].prodPropValues[value[0].prodPropValues.length-1].propValue){
                this.addskuDisplayList[0].prodPropValues.push({})
            }
        },
        editInput(value:any){
            if(value[0].prodPropValues[value[0].prodPropValues.length-1].propValue){
                this.skuDisplayList[0].prodPropValues.push({})
            }
        }
    }
})
export default productStore