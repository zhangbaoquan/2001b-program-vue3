import {defineStore} from 'pinia'
import { toSearchList ,toSeardeleteone} from '../../service/hotsear/toSearch'
const toSearchStore:any=defineStore('toSearch',{
    state(){
        return{
            data:[],
            dialogVisible:false,
            delId:null,
            current:1,
            size:10,
            proSearch:{}
        }
    },
    actions:{
        async getlist(){
            const res:any=await toSearchList({
              t:1666867640113,
              current:this.current,
              size:this.size
            })
            this.data=res.data.records
            console.log('====================================');
            console.log(this.data,"toSeardeleteone");
            console.log('====================================');
        },
        async truedelone (){
            await toSeardeleteone([this.delId])
            this.getlist()
        },
        // searchproList(row:any){
        //     console.log(row)
        // }
    }
})
export default toSearchStore