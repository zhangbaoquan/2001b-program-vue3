import {defineStore} from 'pinia'
import { _del_one ,_get_produce} from '../../service/produce/produce'
const produceStore:any=defineStore('produce',{
    state(){
        return{
            data:[],
            dialogVisible:false,
            delId:null,
            current:1,
            size:10,
            proSearch:{}
        }
    },
    actions:{
        async getlist(){
            const res:any=await _get_produce({
              t:1666867640113,
              current:this.current,
              size:this.size
            })
            this.data=res.data.records
        },
        async truedelone (){
            await _del_one([this.delId])
            this.getlist()
        },
        searchproList(row:any){
            console.log(row)
        }
    }
})
export default produceStore