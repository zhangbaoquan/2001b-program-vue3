import { createApp } from 'vue'
import './style.css'
import App from './App.vue'
import router from './router/router'
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import * as ElementPlusIconsVue from '@element-plus/icons-vue'
import {createPinia} from 'pinia'
import Antd from 'ant-design-vue';
import 'ant-design-vue/dist/antd.css';
//组件
import Avue from '@smallwei/avue';
// import axios from 'axios'
// window.axios = axios;
import '../node_modules/@smallwei/avue/lib/index.css';
import locale from "element-plus/lib/locale/lang/zh-cn"
const pinia = createPinia();
 const app = createApp(App)
 for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
  app.component(key, component);
}
createApp(App).mount('#app');
 app.use(pinia)
 app.use(ElementPlus,{locale});
 app.use(router)
 app.use(Antd)
 app.use(Avue)
 app.mount('#app')
