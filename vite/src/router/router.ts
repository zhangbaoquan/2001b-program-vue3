import { createRouter, createWebHistory } from 'vue-router'
// 类型校验
import type { RouteRecordRaw } from 'vue-router'
const routes: RouteRecordRaw[] = [

    // 重定向
    {
        path: '/',
        redirect: '/login'
    },


    // 最外层的大页面
    {
        path: '/index',
        name: 'index',
        component: () => import('../vivew/index/index.vue'),
        redirect:'/home',
        children: [
            
            {
                path: '/home',
                name: 'home',
                component: () => import('../vivew/home/index.vue')
            },



            {
                path: '/category',
                name: 'category',
                component: () => import('../vivew/prod/category/index.vue')
            },
            {
                path: '/prodComm',
                name: 'prodComm',
                component: () => import('../vivew/prod/prodComm/index.vue')
            },
            {
                path: '/prodList',
                name: 'prodList',
                component: () => import('../vivew/prod/prodList/index.vue')
            },
            {
                path: '/prodlnfo',
                name: 'prodlnfo',
                component: () => import('../vivew/prod/prodlnfo/index.vue')
            },
            {
                path: '/prodTag',
                name: 'prodTag',
                component: () => import('../vivew/prod/prodTag/index.vue')
            },




            {
                path: '/admin',
                name: 'admin',
                component: () => import('../vivew/shop/admin/index.vue')
            },
            {
                path: '/hotSearch',
                name: 'hotSearch',
                component: () => import('../vivew/shop/hotSearch/index.vue')
            },
            {
                path: '/notice',
                name: 'notice',
                component: () => import('../vivew/shop/notice/index.vue')
            },
            {
                path: '/pickAddr',
                name: 'pickAddr',
                component: () => import('../vivew/shop/pickAddr/index.vue')
            },
            {
                path: '/transport',
                name: 'transport',
                component: () => import('../vivew/shop/transport/index.vue')
            },




            {
                path: '/user',
                name: 'user',
                component: () => import('../vivew/user/index.vue')
            },
            {
                path: '/order',
                name: 'order',
                component: () => import('../vivew/order/index.vue')
            },


            

            {
                path: '/area',
                name: 'area',
                component: () => import('../vivew/sys/area/index.vue')
            },
            {
                path: '/config',
                name: 'config',
                component: () => import('../vivew/sys/config/index.vue')
            },
            {
                path: '/log',
                name: 'log',
                component: () => import('../vivew/sys/log/index.vue')
            },
            {
                path: '/menu',
                name: 'menu',
                component: () => import('../vivew/sys/menu/index.vue')
            },
            {
                path: '/role',
                name: 'role',
                component: () => import('../vivew/sys/role/index.vue')
            },
            {
                path: '/schedule',
                name: 'schedule',
                component: () => import('../vivew/sys/schedule/index.vue')
            },

            
            {
                path: '/users',
                name: 'users',
                component: () => import('../vivew/sys/user/index.vue')
            },
        ]
    },


    // 登录页面
    {
        path: '/login',
        name: 'login',
        component: () => import('../vivew/login/index.vue')
    },


    // 404页面
    {
        path: '/:pathMatch(.*)',
        component: () => import('../vivew/404/404.vue')
    },



]



const router = createRouter({
    history: createWebHistory(),
    routes
})


export default router